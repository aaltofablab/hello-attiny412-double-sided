# Hello ATtiny412 Double-Sided

![Hero Shot](double-sided-t412.jpg)

This is a double-sided hello world board featuring the ATtiny412 chip. On top of the [original design by Neil Gershenfeld](http://academy.cba.mit.edu/classes/embedded_programming/t412/hello.t412.echo.png) it includes the following.

- Power LED
- Programmable LED
- Push-Button
- It is double-sided

If you want to see how it is being designed and then made, watch the videos listed below.

- [Designing Double-Sided PCB with KiCAD and Mods](https://youtu.be/ZbP9FEIVfaw)
- [Milling and Soldering Double-Sided PCB with KiCAD, Mods and SRM-20](https://youtu.be/8U3UEAqSkgA)

Please leave any comments in Issues and add upgrades as pull requests. 
